
var regApp = angular.module('regApp')





regApp.service('RegistrationService', function ($http){
	
	this.sendRegistration = function(newreg){
		
		var Request = new XMLHttpRequest();

		Request.open('POST', 'http://private-anon-882c9f460-ionictestapi.apiary-mock.com/registration');

		Request.setRequestHeader('Content-Type', 'application/json');

		Request.onreadystatechange = function () {
		 
		 if (this.readyState === 4 ) {
			
			console.log('---------------------------');
			console.log('Status:', this.status);
			console.log('Headers:', this.getAllResponseHeaders());
			console.log('Body:', this.responseText);
			console.log('name ' + newreg.name);
			console.log('email '+ newreg.email);
			console.log('password '+ newreg.password);
			console.log('password_confirmation '+ newreg.password_confirmation);
			console.log('terms_accepted '+ newreg.terms_accepted);
			if(Request.status == 201)
			{
				alert("Successful registration!");
			}
			else
			{
				alert("OOps! Something went wrong!");
			}
		  }
		};

		var body = {
		  'name':  newreg.name,
		  'email': newreg.email,
		  'password': newreg.password,
		  'password_confirmation': newreg.password_confirmation,
		  'terms_accepted': newreg.terms_accepted
		};

		Request.send(JSON.stringify(body));
		}

})