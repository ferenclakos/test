// Ionic regApp App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
  angular.module('regApp', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})


function CheckNewReg(){
		
		console.log("CheckNewReg is called...");
		var toCheck="";
		var fault = false;
		var name = document.getElementById("name").value;
		if(name == "" || name == undefined)
		{
			console.log("name is missing...");
			document.getElementById("name").classList.add("has-error");
			toCheck = toCheck + "Name\n"
			fault = true;
		}
		else
		{
			document.getElementById("name").classList.remove("has-error");
		}
		
		var pattern = /[^@]+@[^@]+\.[a-zA-Z]{2,6}/;
		var email = document.getElementById("email").value;
		if(!pattern.test(email))
		{
			console.log("e-mail is not correct...");
			document.getElementById("email").classList.add("has-error");
			toCheck = toCheck + "E-mail\n";
			fault = true;
		}
		else
			document.getElementById("email").classList.remove("has-error");
		
		var password = document.getElementById("password").value;
		var password_confirmation = document.getElementById("password_confirmation").value;
		if(password != password_confirmation || password=="" || password_confirmation =="" )
		{
			console.log("password is not correct...");
			document.getElementById("password").classList.add("has-error");
			document.getElementById("password_confirmation").classList.add("has-error");
			toCheck = toCheck + "Password\n";
			fault = true;
		}
		else
		{
			document.getElementById("password").classList.remove("has-error");
			document.getElementById("password_confirmation").classList.remove("has-error");
		}
			
		var terms_accepted = document.getElementById("terms_accepted").checked;
		if(terms_accepted == false)
		{
			console.log("terms have to be accepted...");
			document.getElementById("terms_accepted").classList.add("has-error");
			toCheck = toCheck + "Licence Agreement\n"
			fault = true;
		}
		else
			document.getElementById("terms_accepted").classList.remove("has-error");
		
		if(fault)
		{
			alert("Correct your inputs:\n\n" + toCheck);
		}
		return !fault;
}

