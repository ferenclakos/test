$http({
			method: "POST",
			url: 'http://private-anon-882c9f460-ionictestapi.apiary-mock.com/registration',
			headers: {
				'Content-Type': 'application/json'
			},
			data: {
			  'name': newreg.name,
			  'email': newreg.email,
			  'password': newreg.password,
			  'password_confirmation': newreg.password_confirmation,
			  'terms_accepted': newreg.terms_accepted
			}
		})
		.success( function (res){
			console.log('name ' + newreg.name);
			console.log('email '+ newreg.email);
			console.log('password '+ newreg.password);
			console.log('password_confirmation '+ newreg.password_confirmation);
			console.log('terms_accepted '+ newreg.terms_accepted);
			console.log('success: ' + res);		
		})
		.error(function(err, status){
			console.log('name ' + newreg.name);
			console.log('email '+ newreg.email);
			console.log('password '+ newreg.password);
			console.log('password_confirmation '+ newreg.password_confirmation);
			console.log('terms_accepted '+ newreg.terms_accepted);
			console.log('error: ' + err + ' ' + status);
		})
		