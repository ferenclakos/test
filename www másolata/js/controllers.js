angular.module('starter.controllers', [])

.controller('RegCtrl', function ($scope,$http){
	$scope.data={};
	$scope.data.one="one";
	$scope.data.two = "two";
	
	$scope.sendRegistration = function(){
		
		var Request = new XMLHttpRequest();

		Request.open('POST', 'http://private-anon-882c9f460-ionictestapi.apiary-mock.com/registration');

		Request.setRequestHeader('Content-Type', 'application/json');

		Request.onreadystatechange = function () {
		  if (this.readyState === 4) {
			console.log('--------------------------------------------------------')
			console.log('Status:', this.status);
			console.log('Headers:', this.getAllResponseHeaders());
			console.log('Body:', this.responseText);
			console.log('---');
			console.log('name:', $scope.data.name);
			console.log('email:', $scope.data.email);
			console.log('password:',$scope.data.password);
			console.log('password_confirmation:',$scope.data.password_confirmation);
			console.log('terms_accepted:',$scope.data.licence);
			alert("Registration is sent...");
		  }
		};

		var body = {
		  'name': $scope.data.name,
		  'email': $scope.data.email,
		  'password': $scope.data.password,
		  'password_confirmation': $scope.data.password_confirmation,
		  'terms_accepted': $scope.data.licence
		};

		Request.send(JSON.stringify(body));
		
	}

});