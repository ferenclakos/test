
var regApp = angular.module('regApp')

regApp.controller('RegistrationCtrl', function($scope, RegistrationService){

	$scope.register = function(){
		
		if(RegistrationService.CheckNewReg($scope)){
			RegistrationService.sendRegistration($scope.newreg);
		}
		
	}
})